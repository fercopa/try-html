$(document).ready(function(){
  $('.contrato').css('display', 'none');
  $('#presupuesto').css('display', 'none');
  $('#cobrar').css('display', 'none');
  $('.descarga').css('display', 'none');
  $('.ofrecer').click(function(){
    $('.contrato').css('display', 'block');
    $(this).css('display', 'none');
  });
  $('.contrato').click(function(){
    $('#presupuesto').css('display', 'flex');
    $(this).css('display', 'none');
  });
  $('.accept-presupuesto').click(function(){
    $('#presupuesto').css('display', 'none');
    $('#cobrar').css('display', 'flex');
  });
  $('#cobrar-btn').click(function(){
    $('#cobrar').css('display', 'none');
    $('.descarga').css('display', 'block');
  });
});
