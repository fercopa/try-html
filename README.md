# Run locally
### Python Versions
This repository is tested under Python 3.6-3.7

## Install locally

Install the following library on **Ubuntu 18.x/Debian**: libpq-dev
```sh
sudo apt update
sudo apt install libpq-dev
```

## Install virtualenvwrapper
virtualenvwrapper should be installed into the same 
global site-packages area where virtualenv is installed. 
You may need administrative privileges to do that. The easiest way to install it is using pip

```sh
sudo pip install virtualenvwrapper
```

### Install Docker and Docker compose
On **Ubuntu 18.x/Debian**
```sh
sudo apt install docker.io
```

The Docker service needs to be setup to run at startup. To do so, type in each command followed by enter:

```sh
sudo systemctl start docker
sudo systemctl enable docker
```

Docker Compose:

```sh
sudo apt install docker-compose
```

### Download the repository
```sh
git clone git@gitlab.com:infoxel/tag-x/media-coverage.git
```

## Configuration
### Enviroments
Add to .bashrc or .zshrc file:
```sh
# Virtualenvwrapper
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3 # Optional
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
export PROJECT_HOME=$HOME/Devel
export WORKON_HOME=$HOME/Envs
source /usr/local/bin/virtualenvwrapper.sh
```
After editing it, reload the startup file (e.g., run source ~/.bashrc or ~/.zshrc).

#### Create an enviroment
```sh
cd /path/to/repo/media-coverage
mkvitualenv -a . --python=python3 media-coverage
deactivate
```
Now edit ~/Envs/media-coverage/bin/postactivete
```sh
set -o allexport; source /path/to/repo/media-coverage/environments/local; set +o allexport
export PYTHONPATH=/path/to/repo/media-coverage
export GOOGLE_APPLICATION_CREDENTIALS=/path/to/credentials.json
```
>**NOTE:** The credential.json file is provided by the administrator.

### Install the requirements
Activate the enviroment
```sh
workon media-coverage
```
Now install from requirements directory local.txt, libs.txt, production.txt
```sh
pip install -r requirements/local.txt
pip install -r requirements/libs.txt
pip install -r requirements/production.txt
```

### Run support docker
```sh
cd media-coverage
docker-compose -f containers/compose/docker-compose.yml up
```

If this fail, try with:
```sh
docker-compose -f containers/compose/docker-compose.yml up eslaticsearch pubsub
```
### Make migrations
```sh
python service/manage.py migrate
```
### Run on 8010 port
```sh
python service/manage.py runserver 8010
```
## Now go and install the [Auth](https://gitlab.com/infoxel/tag-x/auth) repository